package edu.demo.knightauto;

import edu.demo.knightauto.knights.Knight;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
public class KnightautoApplication {

	private final Knight knight;

	public static void main(String[] args) {
		SpringApplication.run(KnightautoApplication.class, args);
	}

	@Autowired
	public KnightautoApplication(Knight knight) {
		this.knight= knight;
	}

	@PostConstruct
	private void start() {
		knight.embarkOnQuest();
	}

}
