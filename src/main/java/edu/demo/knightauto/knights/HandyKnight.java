package edu.demo.knightauto.knights;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class HandyKnight implements Knight {
	private final Quest quest;

	@Autowired
	public HandyKnight(Quest quest) {
		this.quest = quest;
	}

	@Override
	public void embarkOnQuest() {
		quest.embark();
	}
}


