package edu.demo.knightauto.knights;


public interface Knight {
    void embarkOnQuest();
}
