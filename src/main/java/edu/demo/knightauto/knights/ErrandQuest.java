package edu.demo.knightauto.knights;

import org.springframework.stereotype.Component;

@Component
public class ErrandQuest implements Quest {

    @Override
    public void embark() {
        System.out.println("Quest>> Destroy this message after reading...");
    }
}
